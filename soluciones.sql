﻿                                                /** MODULO I - UNIDAD II - CONSULTAS DE SELECCION 3 **/

USE ciclistas;

/* 1.1 Listar las edades de todos los ciclistas de Banesto */
SELECT DISTINCT
  c.edad
FROM
  ciclista c
WHERE
  c.nomequipo LIKE 'BANESTO'
;

/* 1.2 Listar las edades de los ciclistas que son de Banesto o de Navigare */
SELECT DISTINCT
  c.edad
FROM
  ciclista c
WHERE
  c.nomequipo LIKE 'BANESTO'
  OR c.nomequipo LIKE 'NAVIGARE'
;

/* 1.3 Listar el dorsal de los ciclistas que son de Banesto y cuya edad esta entre 25 y 32 */
SELECT
  c.dorsal
FROM
  ciclista c
WHERE
  c.nomequipo LIKE 'BANESTO'
  AND edad BETWEEN 25 AND 32
;

/* 1.4 Listar el dorsal de los ciclistas que son de Banesto o cuya edad estae entre 25 y 32 */
SELECT
  c.dorsal
FROM
  ciclista c
WHERE
  c.nomequipo LIKE 'BANESTO'
  OR c.edad BETWEEN 25 AND 32
;

/* 1.5 Listar la inicial del equipo de los ciclistas cuyo nombre comience por R */
SELECT
  LEFT(c.nombre, 1) INICIAL
FROM
  ciclista c
WHERE
  c.nomequipo LIKE 'R%'
;

/* 1.6 Listar el código de las etapas que su salida y llegada sea en la mismma poblacion */
SELECT
  e.numetapa
FROM
  etapa e
WHERE
  e.salida LIKE e.llegada
;

/* 1.7 Listar el código de las etapas que su salida y llegada no sean en la misma población
       y que conozcamos el dorsal del ciclista que ha ganado la etapa */
SELECT
  e.numetapa
FROM
  etapa e
WHERE
  e.salida NOT LIKE e.llegada
  AND e.dorsal IS NOT NULL
;

/* 1.8 Listar el nombre de los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor que 2400 */
SELECT
  p.nompuerto
FROM
  puerto p
WHERE
  p.altura BETWEEN 1000 AND 2000
  OR p.altura > 2400
;

/* 1.9 Listar el dorsal de los ciclistas que hayan ganado los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor que 2400 */
SELECT
  p.dorsal
FROM
  puerto p
WHERE
  p.altura BETWEEN 1000 AND 2000
  OR p.altura > 2400
;

/* 1.10 Listar el número de ciclistas que hayan ganado alguna etapa */
SELECT
  COUNT(e.dorsal)
FROM
  etapa e
;

/* 1.11 Listar el número de etapas que tengan puerto */
SELECT
  COUNT(DISTINCT p.numetapa) EtapasConPuerto
FROM
  puerto p
;

/* 1.12 Listar el número de ciclistas que hayan ganado algún puerto */
SELECT
  COUNT(p.dorsal)
FROM
  puerto p
;

/* 1.13 Listar el código de la etapa con el número de puertos que tiene */
SELECT
  numetapa,
  COUNT(numetapa) nPuertos
FROM
  puerto
    GROUP BY numetapa
;

/* 1.14 Indicar la altura media de los puertos */
SELECT
  AVG(p.altura) AlturaMedia
FROM
  puerto p
;

/* 1.15 Indicar el código de la etapa cuya altura media de sus puertos está por encima de 1500 */
SELECT
  numetapa
FROM
  (SELECT
    numetapa,AVG(altura) media
  FROM
    puerto GROUP BY numetapa) c1 WHERE c1.media > 1500
;

/* 1.16 Indicar el número de etapaas que cumplen la condición anterior */
SELECT COUNT(*) nEtapas FROM (
  SELECT
    numetapa
  FROM
    (SELECT
      numetapa,AVG(altura) media
    FROM
      puerto GROUP BY numetapa) c1 WHERE c1.media > 1500
) c2
;

/* 1.17 Listar el dorsal del ciclista con el  núumero de veces que ha llegando algún maillot */
SELECT
  dorsal,
  COUNT(*) veces
FROM
  lleva
  GROUP BY dorsal
;

/* 1.18 Listar el dorsal del ciclista con el código de maillot y cuantas veces ese ciclista ha llevado ese maillot */
SELECT
  l.dorsal,
  l.código,
  COUNT(*) veces
FROM
  lleva l
  GROUP BY l.dorsal,
           l.código
;

/* 1.19 Listar el dorsal, el código de etapa, el ciclista y el número de maillots que ese ciclista ha llevado en cada etapa */
SELECT
  l.dorsal,
  l.numetapa,
  c.nombre,
  COUNT(*) veces
FROM
  lleva l
  JOIN ciclista c
    ON l.dorsal = c.dorsal
GROUP BY l.dorsal,
         l.numetapa
;